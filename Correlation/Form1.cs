﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using MathNet.Numerics.LinearAlgebra.Factorization;
using MathNet.Numerics.LinearAlgebra;
using MathNet.Numerics;
using MathNet.Numerics.LinearAlgebra.Single;

namespace Correlation
{
    interface word_processor
    {
        String word_process(String word);
        String[] text_process(String[] text);
    }

    public class al_word_processor : word_processor
    {
        public al_word_processor()
        {

        }

        public String word_process(String word)
        {
            String result = word;
            result = result.Replace(")", "").Replace("(", "").Replace("'", "");
            result = result.Replace(",", "").Replace(".", "").Replace("!", "").Replace("?", "");
            result = result.Replace("(", " ").Replace(".", " ").Replace("/", " ").Replace("«", " ").Replace("»", " ");
            result = result.Replace("-", " ").Replace(">", " ").Replace(",", " ").Replace(")", " ");
            result = result.Replace("\\", " ").Replace(":", " ");
            result = result.ToLower();
            while (result[result.Length - 1].Equals(' '))
            {
                result = result.Substring(0, result.Length - 1);
            }
            while (result[0].Equals(' '))
            {
                result = result.Substring(1, result.Length - 1);
            }
            return result;
        }

        public String[] text_process(String[] text)
        {
            return text;
        }
    }
    public partial class Form1 : Form
    {

        private System.Windows.Forms.TextBox editBox;
        List<string[]> all_words = new List<string[]>();
        List<string[]> all_keywords = new List<string[]>();
        List<String> words = new List<String>();
        int itemSelected;
        public Form1()
        {
            InitializeComponent();
            editBox = new System.Windows.Forms.TextBox();
            editBox.Location = new System.Drawing.Point(0, 0);
            editBox.Size = new System.Drawing.Size(0, 0);
            editBox.Hide();
            editBox.Text = "";
            editBox.BackColor = Color.Beige;
            editBox.Font = new Font("Varanda", 15, FontStyle.Regular | FontStyle.Underline, GraphicsUnit.Pixel);
            editBox.ForeColor = Color.Blue;
            editBox.BorderStyle = BorderStyle.FixedSingle;
            editBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EditOver);
            editBox.LostFocus += new System.EventHandler(this.FocusOver);
        }
        private void FocusOver(object sender, System.EventArgs e)
        {
            listBox1.Items[itemSelected] = editBox.Text;
            editBox.Hide();
        }

        private void EditOver(object sender, System.Windows.Forms.KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                listBox1.Items[itemSelected] = editBox.Text;
                editBox.Hide();
            }
        }

        private void label1_Click(object sender, EventArgs e)
        {

        }

        private void label2_Click(object sender, EventArgs e)
        {

        }

        private void textBox1_MouseDoubleClick(object sender, MouseEventArgs e)
        {
            String selection = textBox1.SelectedText;
            if (selection.Equals(" ") == false && selection.Equals("") == false && words.IndexOf(word_proc(selection)) == -1) {
                words.Add(word_proc(selection));
                listBox1.Items.Add(word_proc(selection));
            }
        }

        private String word_proc(String word)
        {
            al_word_processor w = new al_word_processor();
            return w.word_process(word);
        }
        private void button1_Click(object sender, EventArgs e)
        {
            String ptext = textBox1.Text;
            ptext = ptext.Replace("(", " ").Replace(".", " ").Replace("/", " ").Replace("«", " ").Replace("»", " ");
            ptext = ptext.Replace("-", " ").Replace(">", " ").Replace(",", " ").Replace(")", " ");
            ptext = ptext.Replace("\\", " ").Replace(":", " ");
            ptext = ptext.ToLower();
            String[] new_words = ptext.Split().Select(x => x.TrimEnd(",.;:-".ToCharArray())).ToArray();
            for (int i = 0; i < new_words.Length; ++i)
            {
                if (new_words[i].Equals(" ") == false && new_words[i].Equals("") == false)
                    new_words[i] = word_proc(new_words[i]);
            }
            all_words.Add(new_words);
            List<String> new_add = new List<String>();
            for (int i = 0; i < listBox1.Items.Count; i++)
            {
                new_add.Add(listBox1.Items[i].ToString());
            }
            all_keywords.Add(new_add.ToArray());
            words.Clear();
            textBox1.Clear();
            listBox1.Items.Clear();
            numericUpDown1.Value = numericUpDown1.Value + 1;

        }

        private void textBox2_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyCode == Keys.Enter)
            {
                String new_word = word_proc(textBox2.Text);
                if (words.IndexOf(new_word) == -1)
                {
                    words.Add(new_word);
                    listBox1.Items.Add(new_word);
                }
            }
        }

        private void listBox1_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F2)
                CreateEditBox(sender);
        }

        private void listBox1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
                CreateEditBox(sender);
        }

        private void listBox1_DoubleClick(object sender, EventArgs e)
        {
            CreateEditBox(sender);
        }

        private void CreateEditBox(object sender)
        {
            if (listBox1.SelectedIndex != -1)
            {


                listBox1 = (ListBox)sender;
                itemSelected = listBox1.SelectedIndex;
                Rectangle r = listBox1.GetItemRectangle(itemSelected);
                string itemText = (string)listBox1.Items[itemSelected];
                int delta = 0;
                editBox.Location = new System.Drawing.Point(r.X + delta, r.Y + delta);
                editBox.Size = new System.Drawing.Size(r.Width - 10, r.Height - delta);
                editBox.Show();
                listBox1.Controls.AddRange(new System.Windows.Forms.Control[] { this.editBox });
                editBox.Text = itemText;
                editBox.Focus();
                editBox.SelectAll();
                editBox.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.EditOver);
                editBox.LostFocus += new System.EventHandler(this.FocusOver);
            }
        }

        private void contextMenuStrip1_Opening(object sender, CancelEventArgs e)
        {

        }

        private void toolStripMenuItem1_Click(object sender, EventArgs e)
        {
            if (listBox1.SelectedIndex != -1)
            {
                String deleted = (String)listBox1.Items[listBox1.SelectedIndex];
                if (words.Contains(deleted))
                {
                    words.Remove(deleted);
                }
                listBox1.Items.RemoveAt(listBox1.SelectedIndex);
            }
        }

        private void textBox1_KeyUp(object sender, KeyEventArgs e)
        {
            if (e.KeyData == Keys.F2)
            {
                String selection = textBox1.SelectedText;
                if (words.IndexOf(word_proc(selection)) == -1)
                {
                    words.Add(word_proc(selection));
                    listBox1.Items.Add(word_proc(selection));
                }
            }
        }

        private List<String> get_unic()
        {
            List<String> unic_key = new List<String>();
            for (int i = 0; i < all_keywords.Count; ++i)
            {
                for (int j = 0; j < all_keywords[i].Length; ++j)
                {
                    if (unic_key.Contains(all_keywords[i][j]) == false)
                    {
                        unic_key.Add(all_keywords[i][j]);
                    }
                }
            }
            return unic_key;
        } //++

        private double[][] create_matrix(List<String> unic_key)
        {
            int req = all_words.Count;
            int unic = unic_key.Count;
            double[][] matrix = new double[req][];
            for (int i = 0; i < req; ++i)
            {
                matrix[i] = new double[unic];
                for (int j = 0; j < unic; ++j)
                    matrix[i][j] = 0.0;
            }
            return matrix;
        }
        private Matrix<double> get_Matrix()
        {
            List<String> unic_key = get_unic();
            int req = all_words.Count;
            int unic = unic_key.Count;
            double[][] mat = create_matrix(unic_key);

            for (int i = 0; i < req; ++i)
            {
                for (int j = 0; j < unic; ++j)
                {
                    for (int z = 0; z < all_words[i].Length; ++z)
                    {
                        if (all_words[i][z].Equals(unic_key[j]))
                            mat[i][j] = mat[i][j] + 1.0;
                    }
                }
            }
            Matrix<double> M = Matrix<Double>.Build.DenseOfColumns(mat);
            var sums = M.ColumnSums();
            for (int i = 0; i < sums.Count; ++i)
            {
                for (int j = 0; j < M.RowCount; ++j)
                {
                    if (sums[i] == 0.0)
                        M[j, i] = 0.0;
                    else
                        M[j, i] = M[j, i] / sums[i];
                }
            }


            return M;
        }

        private void button2_Click(object sender, EventArgs e)
        {

            int req = all_words.Count;
            List<String> unic_key = new List<String>();
            List<String> unic_key_double = new List<String>();
            for (int i = 0; i < all_keywords.Count; ++i)
            {
                for (int j = 0; j < all_keywords[i].Length; ++j)
                {
                    if (unic_key.Contains(all_keywords[i][j]) == false)
                    {
                        unic_key.Add(all_keywords[i][j]);
                    }
                    else
                        if (unic_key_double.Contains(all_keywords[i][j]) == false)
                        unic_key_double.Add(all_keywords[i][j]);
                }
            }//++
            int unic = unic_key_double.Count;
            Matrix<double> mat = CreateMatrix.Dense(req, unic, 0.0);
            for (int i = 0; i < req; ++i)
            {
                for (int j = 0; j < unic; ++j)
                {
                    for (int z = 0; z < all_words[i].Length; ++z)
                    {
                        if (all_words[i][z].Equals(unic_key_double[j])) {
                            mat[i, j] = mat[i, j] + 1.0;
                        }
                    }
                }
            }

            var sums = mat.ColumnSums();
            //int zx = 0;
            //int zx2 = 3 / zx;
            for (int i = 0; i < mat.RowCount; ++i)
            {
                for (int j = 0; j < mat.ColumnCount; ++j)
                {
                    //if(sums[j] != 0.0)
                    //mat[i, j] = mat[i, j] / sums[j];
                    //else
                    //{
                    //mat[i, j] = 0;
                    //}
                }
            }

            //"______________________"
            Matrix<double> M = mat;
            Matrix_reduction mr = new Matrix_reduction(M);
            Matrix<double> reducted = mr.lower_rank(req, unic);
            
            double[][] result = new double[req][];
            for (int i = 0; i < req; ++i)
            {
                result[i] = new double[i + 1];
                for (int j = 0; j < i + 1; ++j)
                    result[i][j] = MathNet.Numerics.Statistics.Correlation.Pearson(reducted.Row(i), reducted.Row(j));
            }
            ResultForm rf = new ResultForm(this, result);
            this.Hide();
            rf.Show();
        }
    }

    public class Matrix_reduction{
        Matrix<double> M;
        public Matrix_reduction(Matrix<double> Mat)
        {
            this.M = Mat;
        }

        public Matrix<double> lower_rank(int req, int unic)
        {

            var factorSvd = M.Svd(true);
            int k = M.Rank();
            k = req;
            if (k > unic)
                k = unic;
            var U = factorSvd.U.SubMatrix(0, factorSvd.U.RowCount, 0, k);
            var V = factorSvd.VT.SubMatrix(0, k, 0, factorSvd.VT.ColumnCount);
            var SS = factorSvd.S.SubVector(0, k);
            var S = CreateMatrix.SparseDiagonal(k, k, 0.0);
            for (int i = 0; i < k; ++i)
            {
                S[i, i] = SS[i];
            }
            // S = S.SetDiagonal(SS);
            var s1 = S;
            //s1.SetDiagonal(factorSvd.S.SubVector(0, k));
            //var s2 = V.Multiply(S);
            //var s3 = U.Multiply(s2);
            U = U.Multiply(S);
            U = U.Multiply(V);
            return U;
        }
        }

}

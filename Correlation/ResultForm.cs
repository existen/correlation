﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Correlation
{
    public partial class ResultForm : Form
    {
        Form pform;
        double[][] result;
        public ResultForm(Form pform, double[][] result)
        {
            InitializeComponent();
            this.pform = pform;
            this.result = result;
            dataGridView1.Columns.Add("Index", "Index");
            for (int i = 0; i < result.Length; ++i)
                dataGridView1.Columns.Add((i+1).ToString(), (i + 1).ToString());
            for (int i = 0; i < result.Length; ++i)
            {
                String[] newRow = new String[result.Length + 1];
                newRow[0] = (i + 1).ToString();
                for (int j = 0; j < i + 1; ++j)
                {
                    newRow[j + 1] = result[i][j].ToString();
                }
                dataGridView1.Rows.Add(newRow);

            }
            for(int i = 0; i < result.Length; i++)
            {
                for(int j = 1; j < result[i].Length+1; ++j)
                {
                    if (i != j - 1)
                    {
                        if (Math.Abs(result[i][j - 1]) > 0.25)
                        {
                            if (Math.Abs(result[i][j - 1]) > 0.75)
                                dataGridView1.Rows[i].Cells[j].Style.BackColor = Color.Green;
                            else
                                dataGridView1.Rows[i].Cells[j].Style.BackColor = Color.Yellow;
                        }
                    }
                }
            }

        }
    }
}
